import java.io.IOException;
import lts.Integrator;
import lts.LTSException;

/**
 *
 * @author BeanToan
 */
public class Agltsa {
    Integrator integrator;
    
    boolean isWriteResult;
            
    public Agltsa(String fspFilePath) {
        this.integrator = new Integrator(fspFilePath);
        
        this.isWriteResult = false;
    }

    public Agltsa(String fspFilePath, boolean isWriteResult) {
        this(fspFilePath);
        this.isWriteResult = isWriteResult;
    }
    
    /**
     * Analyze the content of FSP file
     */
    protected void analyze() {        
        boolean validFiles = integrator.validateFiles();
                
        if (validFiles) {
            /**
             * Read FSP file
             */
            try {
                integrator.readFsp();
            } catch (IOException e) {
                Integrator.printResult(Integrator.FAILURE, "Error in reading LTS file. " + e.getMessage());
                return;
            }
            
            String fspContent = integrator.getFspContent();
            
            /**
             * Compile FSP file's content
             */
            try {
                integrator.compileFsp(); 
            } catch (LTSException e) {
                Integrator.printError(e, fspContent);
                return;
            }

            /**
             * Generate FSP into the listing form format
             */
            try {    
                integrator.genListingFormFormat();
            } catch (LTSException e) {
                Integrator.printError(e, fspContent);
                return;
            }
            
            /**
             * Write the listing form format to file
             */
            if (this.isWriteResult) {
                try {    
                    integrator.writeListingFormFormat(integrator.toListingFormFormat());
                } catch (Exception e) {}
            }

            /**
             * Print the listing form to console
             */
            try {    
                Integrator.printResult(Integrator.SUCCESS, integrator.toResultFormat());
            } catch (LTSException e) {
                Integrator.printError(e, fspContent);
            } catch (NullPointerException e) {
                Integrator.printResult(Integrator.FAILURE, "Error in generating LTS " + e.getMessage());
            }
        } else {
            Integrator.printResult(Integrator.FAILURE, "Please provide the correct input file paths");
        }
    }

    /**
     * Main function
     * 
     * @param args 
     */
    public static void main(String[] args) {
        
//        if (args.length >= 1) {
//            boolean isWriteResult = false;
//            
//            if (args.length >= 2) {
//                isWriteResult = "1".equals(args[1]);
//            }
//            
//            Agltsa agltsa = new Agltsa(args[0], isWriteResult);
//            agltsa.analyze();
//        } else {
//            Integrator.printResult(Integrator.FAILURE, "Please provide the input/ouput file paths");
//        }
        
        Agltsa agltsa = new Agltsa("D:\\Workspaces\\Eclipse\\agtool\\examples\\fsp\\Writer.lts", true);
        agltsa.analyze();

        System.exit(0);
    }
}
