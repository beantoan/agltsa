/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agltsa;

/**
 *
 * @author BeanToan
 */
public class LTSInputString implements lts.LTSInput {
    private String fSrc;
    private int fPos;

    public LTSInputString(String string) {
        this.fSrc = string;
        this.fPos = -1;
    }
    
    @Override
    public char nextChar() {
        this.fPos += 1;
        if (this.fPos < this.fSrc.length()) {
            return this.fSrc.charAt(this.fPos);
        }

        return '\000';
    }


    @Override
    public char backChar() {
        this.fPos -= 1;
        if (this.fPos < 0) {
            this.fPos = 0;
            return '\000';
        }

        return this.fSrc.charAt(this.fPos);
    }


    @Override
    public int getMarker() {
        return this.fPos;
    }

    /**
     * @param fSrc the fSrc to set
     */
    public void setfSrc(String fSrc) {
        this.fSrc = fSrc;
        this.fPos = -1;
    }
}
