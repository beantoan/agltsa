/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agltsa;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author BeanToan
 */
public class ListingForm {

    private String initState;
    private List<String> labels;
    private List<String> states;
    private List<StateTransition> transactions;

    public ListingForm() {
    }

    public ListingForm(String initState, List<String> labels, List<String> states, List<StateTransition> transactions) {
        this.initState = initState;
        this.labels = labels;
        this.states = states;
        this.transactions = transactions;
    }

    /**
     * @return the initState
     */
    public String getInitState() {
        return initState;
    }

    /**
     * @param initState the initState to set
     */
    public void setInitState(String initState) {
        this.initState = initState;
    }

    /**
     * @return the transactions
     */
    public List<StateTransition> getTransactions() {
        return transactions;
    }

    /**
     * @param transactions the transactions to set
     */
    public void setTransactions(List<StateTransition> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        if (transactions != null) {
            String[] strTransitions = new String[transactions.size()];

            int i = 0;
            for (StateTransition transition : transactions) {
                strTransitions[i++] = transition.toString();
            }

            return StringUtils.join(strTransitions, ",") + ";" + initState + ".\n";
        }

        return "";
    }
    
    /**
     * Generate the listing form to AGTool format
     * @return 
     */
    public String toResult() {
        if (transactions != null) {
            String[] strTransitions = new String[transactions.size()];

            int i = 0;
            for (StateTransition transition : transactions) {
                strTransitions[i++] = transition.toResult();
            }

            return initState + "\n" + StringUtils.join(states, ",") + "\n" + StringUtils.join(strTransitions, ";");
        }

        return "";
    }

    /**
     * @return the labels
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * @param labels the labels to set
     */
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    /**
     * @return the states
     */
    public List<String> getStates() {
        return states;
    }

    /**
     * @param states the states to set
     */
    public void setStates(List<String> states) {
        this.states = states;
    }
    
    /**
     * Optimize the labels of transitions
     */
    public void optimizeTransitions() {
        if (this.transactions != null && !this.transactions.isEmpty()) {            
            List<StateTransition> optimizedTransitions = new ArrayList<StateTransition>();
            
            for (StateTransition stateTransition : this.transactions) {
                boolean exists = false;
                                
                for (StateTransition stateTransition1 : optimizedTransitions) {
                    if (!stateTransition.getId().equals(stateTransition1.getId())) {
                        if (stateTransition.getStartState().equals(stateTransition1.getStartState()) &&
                                stateTransition.getEndState().equals(stateTransition1.getEndState())) {
                            
                            List<String> newLabels = new ArrayList<String>();
                            newLabels.addAll(stateTransition1.getLabels());
                            newLabels.addAll(stateTransition.getLabels());
                            stateTransition1.setLabels(newLabels);
                            
                            exists = true;
                        }
                    }
                }
                
                if (!exists) {
                    optimizedTransitions.add(stateTransition);
                }
            }
            
            this.transactions = optimizedTransitions;
        }        
    }
}
