/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agltsa;

import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author BeanToan
 */
public class StateTransition {

    private String id;
    private String startState;
    private List<String> labels;
    private String endState;

    public StateTransition() {
        this.id = String.valueOf(java.util.UUID.randomUUID());
    }

    public StateTransition(String startState, List<String> labels, String endState) {
        this.id = String.valueOf(java.util.UUID.randomUUID());
        
        this.startState = startState;
        this.labels = labels;
        this.endState = endState;
    }
    
    public String getId() {
        return this.id;
    }

    /**
     * @return the startState
     */
    public String getStartState() {
        return startState;
    }

    /**
     * @param startState the startState to set
     */
    public void setStartState(String startState) {
        this.startState = startState;
    }

    /**
     * @return the labels
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * @param labels the label to set
     */
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    /**
     * @return the endState
     */
    public String getEndState() {
        return endState;
    }

    /**
     * @param endState the endState to set
     */
    public void setEndState(String endState) {
        this.endState = endState;
    }

    /**
     * Join labels into a string for display
     * @return 
     */
    public String genLabel() {        
        if (this.labels != null && !this.labels.isEmpty()) {
            if (this.labels.size() > 1) {
                return "{" + StringUtils.join(this.labels, "*") + "}";
            } else {
                return this.labels.get(0);
            }
        }
        
        return "";
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append("(");
        builder.append(getStartState());
        builder.append(",");
        builder.append(this.genLabel());
        builder.append(",");
        builder.append(getEndState());
        builder.append(")");

        return builder.toString();
    }
    
    public String toResult() {
        StringBuilder builder = new StringBuilder();

        builder.append(getStartState());
        builder.append(",");
        builder.append(this.genLabel());
        builder.append(",");
        builder.append(getEndState());

        return builder.toString();
    }
}