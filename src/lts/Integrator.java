/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lts;

import agltsa.LTSInputString;
import agltsa.LTSOutputString;
import agltsa.ListingForm;
import agltsa.StateTransition;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author BeanToan
 */
public class Integrator {
    public static final String SUCCESS = "1";
    public static final String FAILURE = "0";
    public static final String FSP_DELIMITER = ".";

    private LTSCompiler ltsCompiler = null;
    private CompositeState compositeState = null;
    private File fspFile = null;
    private File ltsFile = null;
    private String fspContent = null;
    
    private List<ListingForm> listingForms;
    
    private LTSInputString input;
    private LTSOutputString output;

    public Integrator(String fspFilePath) {
        SymbolTable.init();
        
        this.fspFile = new File(fspFilePath);
        this.ltsFile = new File(FilenameUtils.getFullPath(fspFilePath) + FilenameUtils.getBaseName(fspFilePath) + ".compiled");
        
        input = new LTSInputString("\n");
        output = new LTSOutputString();
        
        this.ltsCompiler = new LTSCompiler(input, output, null);
    }

    public Integrator(String fspFilePath, String ltsFilePath) {
        SymbolTable.init();
        
        this.fspFile = new File(fspFilePath);
        this.ltsFile = new File(ltsFilePath);
        
        input = new LTSInputString("\n");
        output = new LTSOutputString();
        
        this.ltsCompiler = new LTSCompiler(input, output, null);
    }

    /**
     * Check the input file and delete the out put file
     *
     * @return
     */
    public boolean validateFiles(){
        if (this.fspFile == null || !this.fspFile.exists()) {
            return false;
        }
        
        try {
            if (this.ltsFile != null && this.ltsFile.exists()) {
                this.ltsFile.deleteOnExit();
            }
        } catch (Exception e){}

        return true;
    }

    /**
     * Read the content of FSP file
     */
    public void readFsp() throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(this.fspFile));

        StringBuilder builder = new StringBuilder();
        String line = null;
        while((line = reader.readLine()) != null) {
            builder.append(line).append("\n");
        }
        
        this.fspContent = builder.toString();
        this.input.setfSrc(this.fspContent);
        
        reader.close();
    }

    /**
     * Write LTS to file under Aldebaran format
     *
     * A .aut file describes a LTS in the Aldebaran format. Each state is
     * represented by a natural number. The first line of filename.aut, called
     * descriptor has the following structure: des (<first-state>,
     * <number-of-transitions>, <number-of-states>) The first state is always
     * equal to 0. Each remaining line of the file represents an edge; these
     * lines have the following structure: (<from-state>, <gate-name>,
     * <to-state>) where <from-state> and <to-state> are numbers and <gate-name>
     * is a character string enclosed in double quotes (with up to 5000
     * characters). Note: <gate-name> is case-sensitive.
     */
    public void writeAldebaranFormat() throws IOException{
        FileOutputStream localFileOutputStream = new FileOutputStream(this.ltsFile);
        PrintStream localPrintStream = new PrintStream(localFileOutputStream);
        this.compositeState.composition.printAUT(localPrintStream);
        localPrintStream.close();
        localFileOutputStream.close();
    }

    /**
     * Write to listing form format
     */
    public void writeListingFormFormat(String listingFormStr) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(this.ltsFile));
        writer.write(listingFormStr, 0, listingFormStr.length());
        writer.close();
    }
    
    /**
     * Generate ListingForm
     * 
     * @return 
     */
    public void genListingFormFormat() throws LTSException, NullPointerException{
        if (this.compositeState == null || this.compositeState.machines == null) {
            return;
        }

        listingForms = new ArrayList<ListingForm>();

        Enumeration localObject = (Enumeration) this.compositeState.machines.elements();

        while (localObject.hasMoreElements()) {

            CompactState compactState = ((CompactState) localObject.nextElement());

            List<String> labels = new ArrayList<String>();
            List<String> states = new ArrayList<String>();
            List<StateTransition> transitions = new ArrayList<StateTransition>();
            
            for (int i = 0; i < compactState.states.length; i++) {
                String startState = String.valueOf(i);
                
                if (states.indexOf(startState) == -1) {
                    states.add(startState);
                }
                
                EventState eventState1 = compactState.states[i];

                while (eventState1 != null) {
                    EventState eventState2 = eventState1;

                    while (eventState2 != null) {
                        String label = compactState.alphabet[eventState2.event];
                        
                        if (labels.indexOf(label) == -1) {
                            labels.add(label);
                        }
                        
                        String endState = String.valueOf(eventState2.next);
                        
                        if (states.indexOf(endState) == -1) {
                            states.add(endState);
                        }
                        
                            transitions.add(new StateTransition(startState, Arrays.asList(label), endState));

                        eventState2 = eventState2.nondet;
                    }

                    eventState1 = eventState1.list;
                }
            }
            
            ListingForm ltf = new ListingForm("0", labels, states, transitions);
            ltf.optimizeTransitions();

            this.listingForms.add(ltf);
        }
        
//        Collections.reverse(this.listingForms);
    }
    
    /**
     * Generate normal ListingForm format
     * @return 
     */
    public String toListingFormFormat() {
        
        String listingFormStr = "";
        
        for (ListingForm listingForm : this.listingForms) {
            listingFormStr += listingForm.toString() + "\n";
        }
        
        return listingFormStr;
    }
    
    /**
     * Generate the readable format for AGTool
     * @return 
     */
    public String toResultFormat() {
        String listingFormStr = "";
        
        for (ListingForm listingForm : this.listingForms) {
            listingFormStr += listingForm.toResult() + "\n";
        }
        
        return listingFormStr;
    }
    
    public Hashtable parseFsp() {
        Hashtable hashtable1 = new Hashtable();
        Hashtable hashtable2 = new Hashtable();
        try {
          this.ltsCompiler.parse(hashtable1, hashtable2);
        } catch (LTSException localLTSException) {
          hashtable1.put("DEFAULT", "DEFAULT");
        }
        
        return hashtable1;
    }
    
    /**
     * Compile FSP content
     */
    public void compileFsp() throws LTSException{        
        this.compositeState = this.ltsCompiler.compile("DEFAULT");
    }

    /**
     * Composite the FSPs
     */
    public void composeFsp() throws LTSException{
        this.compositeState.compose(this.output);
    }

    /**
     * @return the String[] fspContent
     */
    public String[] getFspContents(String delimiter) {
        return StringUtils.split(this.fspContent, delimiter);
    }
    
    /**
     * @return the String[] fspContent
     */
    public String[] getFspContents() {
        return StringUtils.split(this.fspContent, Integrator.FSP_DELIMITER);
    }

    /**
     * @param fspContent the fspContent to set
     */
    public void setFspContent(String fspContent) {
        this.fspContent = fspContent;
    }
    
    /**
     * Return FSPContent
     * 
     * @return 
     */
    public String getFspContent() {
        return this.fspContent;
    }
    
    /**
     * Print result or error message to console
     * 
     * @param status
     * - 1 -> success, 0 -> failure
     * @param result 
     */
    public static void printResult(String status, String result) {
        System.out.println(status);
        System.out.print(result);
        System.out.close();
    }
    
    /**
     * Pretty print
     *
     * @param obj
     */
    public static void printObj(Object obj) {
        System.out.println(ToStringBuilder.reflectionToString(obj, ToStringStyle.MULTI_LINE_STYLE));
    }

    /**
     * Display error when compiling
     * 
     * @param paramLTSException 
     */
    public static void printError(LTSException ltsException, String fspContent) {
        System.out.println(Integrator.FAILURE);
        
        if (ltsException.marker != null) {
            int i = ((Integer) ltsException.marker).intValue();
            int j = 1;
            for (int k = 0; k < i; k++) {
                if (fspContent.charAt(k) == '\n') {
                    j++;
                }
            }
            
            System.out.println("ERROR line:" + j + " - " + ltsException.getMessage());
        } else {
            System.out.println("ERROR - " + ltsException.getMessage());
        }
        
        System.out.close();
    }

    /**
     * @return the listingForms
     */
    public List<ListingForm> getListingForms() {
        return listingForms;
    }

    /**
     * @param listingForms the listingForms to set
     */
    public void setListingForms(List<ListingForm> listingForms) {
        this.listingForms = listingForms;
    }
}
